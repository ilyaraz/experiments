all:
	g++ -std=c++11 -Wall -O3 -march=native solver.cc -o solver -I ../main/FALCONN/src/include -I ../main/FALCONN/external/eigen -fopenmp
	g++ -std=c++11 -Wall -O3 -march=native solver_centering.cc -o solver_centering -I ../main/FALCONN/src/include -I ../main/FALCONN/external/eigen -fopenmp
