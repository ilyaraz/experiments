from math import ceil

print 'rm -rf log'
num_bits = 16
num_tables = 1
while num_tables <= 1300:
    print './solver_centering {} {} >> log'.format(num_bits, num_tables)
    num_tables = int(ceil(num_tables * 1.1))
