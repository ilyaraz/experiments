to_sort = []
for line in open('towhee.txt'):
    tok = line.split()
    data = {}
    for (u, v) in zip(tok[::2], tok[1::2]):
        data[u] = float(v)
    to_sort.append((-data['accuracy'], data['query_time'], data))
pareto = []
best = 1e100
for (u, v, w) in sorted(to_sort):
    if v < best:
        best = v
        pareto.append(w)
for x in reversed(pareto):
    print x['accuracy'], x['query_time'], x['num_bits'], x['num_tables']
