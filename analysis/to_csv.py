import csv
import sys

header = None
data = []

with open(sys.argv[1]) as f:
    for line in f:
        tok = line.split()
        cur_header = tok[::2]
        if header is None:
            header = cur_header
        else:
            assert cur_header == header
        data.append(tok[1::2])
with open(sys.argv[2], 'wb') as f:
    writer = csv.writer(f)
    writer.writerow(header)
    for row in data:
        writer.writerow(row)
