#include <falconn/lsh_nn_table.h>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <map>
#include <random>
#include <set>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <cstdio>

using std::cerr;
using std::ceil;
using std::cout;
using std::endl;
using std::exception;
using std::make_pair;
using std::max;
using std::mt19937_64;
using std::ostream;
using std::pair;
using std::runtime_error;
using std::set;
using std::string;
using std::uniform_int_distribution;
using std::vector;

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;

using falconn::construct_table;
using falconn::compute_number_of_hash_functions;
using falconn::DenseVector;
using falconn::DistanceFunction;
using falconn::LSHConstructionParameters;
using falconn::LSHFamily;
using falconn::LSHNearestNeighborTable;
using falconn::get_default_parameters;
using falconn::core::CrossPolytopeHashDense;

typedef DenseVector<float> Point;

bool read_point(FILE *file, Point *point) {
  int d;
  if (fread(&d, sizeof(int), 1, file) != 1) {
    return false;
  }
  float *buf = new float[d];
  if (fread(buf, sizeof(float), d, file) != (size_t)d) {
    throw runtime_error("can't read a point");
  }
  point->resize(d);
  for (int i = 0; i < d; ++i) {
    (*point)[i] = buf[i];
  }
  delete[] buf;
  return true;
}

bool read_answer(FILE *file, vector<int> *point) {
  int d;
  if (fread(&d, sizeof(int), 1, file) != 1) {
    return false;
  }
  point->resize(d);
  if (fread(&((*point)[0]), sizeof(int), d, file) != (size_t)d) {
    throw runtime_error("can't read an answer");
  }
  return true;
}

void read_dataset(string file_name, vector<Point> *dataset) {
  FILE *file = fopen(file_name.c_str(), "rb");
  if (!file) {
    throw runtime_error("can't open the file with the dataset");
  }
  Point p;
  dataset->clear();
  while (read_point(file, &p)) {
    dataset->push_back(p);
  }
  if (fclose(file)) {
    throw runtime_error("fclose() error");
  }
}

void read_answers(string file_name, vector<vector<int>> *dataset) {
  FILE *file = fopen(file_name.c_str(), "rb");
  if (!file) {
    throw runtime_error("can't open the file with the dataset");
  }
  vector<int> p;
  dataset->clear();
  while (read_answer(file, &p)) {
    dataset->push_back(p);
  }
  if (fclose(file)) {
    throw runtime_error("fclose() error");
  }
}

void normalize(vector<Point> *dataset) {
  for (size_t i = 0; i < dataset->size(); ++i) {
    (*dataset)[i].normalize();
  }
}

pair<int, int> get_kl(int d, int bits) {
  int dcp = d;
  while (dcp & (dcp - 1)) ++dcp;
  int bits_cp = 0;
  while ((1 << bits_cp) < dcp) {
    ++bits_cp;
  }
  ++bits_cp;
  int k = bits / bits_cp;
  int ld = -1;
  if (bits % bits_cp) {
    ++k;
    ld = 1 << ((bits % bits_cp) - 1);
  }
  else {
    ld = dcp;
  }
  return make_pair(k, ld);
}

struct Table {
  vector<uint8_t> start_buf;
  vector<uint8_t> points_buf;

  uint64_t get_memory_usage() {
    return sizeof(uint8_t) * start_buf.size() + sizeof(uint8_t) * points_buf.size();
  }
};

struct Statistics {
  size_t num_bits;
  size_t num_tables;
  double query_time;
  double hash_time;
  double table_time;
  double distance_time;
  double num_candidates;
  double accuracy;
  size_t hash_memory;
  size_t tables_memory;
};

ostream& operator<<(ostream &os, const Statistics &s) {
  os << "num_bits " << s.num_bits << " "
     << "num_tables " << s.num_tables << " "
     << "query_time " << s.query_time << " "
     << "hash_time " << s.hash_time << " "
     << "table_time " << s.table_time << " "
     << "distance_time " << s.distance_time << " "
     << "num_candidates " << s.num_candidates << " "
     << "accuracy " << s.accuracy << " "
     << "hash_memory " << s.hash_memory << " "
     << "tables_memory " << s.tables_memory << " ";
  return os;
}

Statistics evaluate(const vector<Point> &dataset,
		    const Point &center,
		    const vector<Point> &original_queries,
		    const vector<vector<int>> &answers,
		    size_t num_bits,
		    size_t num_tables) {
  Statistics result_;
  result_.num_bits = num_bits;
  result_.num_tables = num_tables;
  const int NUM_THREADS = 4;
  const uint32_t BYTE_WIDTH = 3;
  assert((size_t)1 << (8 * BYTE_WIDTH) > dataset.size());
  assert((size_t)1 << (8 * BYTE_WIDTH) > (size_t)1 << num_bits);
  pair<int, int> aux = get_kl(dataset[0].size(), num_bits);
  int K = aux.first;
  int LD = aux.second;
  vector<CrossPolytopeHashDense<>>
    hasher
    (NUM_THREADS,
     CrossPolytopeHashDense<>(dataset[0].size(),
			      K,
			      num_tables,
			      1,
			      LD,
			      4057218));
  vector<vector<uint32_t>> result(NUM_THREADS);
  vector<vector<uint32_t>> hashes(num_tables, vector<uint32_t>(dataset.size()));
  result_.hash_memory = dataset.size() * num_tables * sizeof(hashes[0][0]);
#pragma omp parallel for num_threads(NUM_THREADS)
  for (size_t i = 0; i < dataset.size(); ++i) {
    int thread_id = omp_get_thread_num();
    hasher[thread_id].hash(dataset[i], &result[thread_id]);
    for (size_t j = 0; j < num_tables; ++j) {
      hashes[j][i] = result[thread_id][j];
    }
  }
  vector<Table> tables(num_tables);
  vector<vector<uint32_t>> _start(NUM_THREADS);
  for (size_t i = 0; i < num_tables; ++i) {
    int thread_id = 0;
    _start[thread_id].clear();
    _start[thread_id].resize((1 << num_bits) + 1, 0);
    tables[i].points_buf.resize(dataset.size() * BYTE_WIDTH);
    for (size_t j = 0; j < dataset.size(); ++j) {
      ++_start[thread_id][hashes[i][j] + 1];
    }
    for (size_t j = 1; j <= ((size_t)1 << num_bits); ++j) {
      _start[thread_id][j] += _start[thread_id][j - 1];
    }
    for (size_t j = 0; j < dataset.size(); ++j) {
      uint32_t cur = _start[thread_id][hashes[i][j]]++;
      cur *= BYTE_WIDTH;
      uint32_t tmp = j;
      for (size_t k = 0; k < BYTE_WIDTH; ++k) {
	tables[i].points_buf[cur++] = tmp & 255;
	tmp /= 256;
      }
    }
    for (size_t j = 1 << num_bits; j >= 1; --j) {
      _start[thread_id][j] = _start[thread_id][j - 1];
    }
    _start[thread_id][0] = 0;
    tables[i].start_buf.resize(((1 << num_bits) + 1) * BYTE_WIDTH);
    for (size_t j = 0; j <= (size_t)1 << num_bits; ++j) {
      uint32_t cur = BYTE_WIDTH * j;
      uint32_t tmp = _start[thread_id][j];
      for (size_t k = 0; k < BYTE_WIDTH; ++k) {
	tables[i].start_buf[cur++] = tmp & 255;
	tmp /= 256;
      }
    }
    vector<uint32_t> aux;
    hashes[i].swap(aux);
  }
  uint64_t total = 0;
  for (size_t i = 0; i < num_tables; ++i) {
    total += tables[i].get_memory_usage();
  }
  result_.tables_memory = total;
  for (int it = 0; it < 5; ++it) {
    vector<pair<float, uint32_t>> candidates;
    vector<Point> queries(original_queries);
    vector<uint32_t> flag(dataset.size(), queries.size());
    int score = 0;
    result_.hash_time = 0.0;
    result_.table_time = 0.0;
    result_.distance_time = 0.0;
    result_.num_candidates = 0.0;
    auto t1 = high_resolution_clock::now();
    for (size_t i = 0; i < queries.size(); ++i) {
      queries[i].normalize();
      queries[i] -= center;
      auto ttt1 = high_resolution_clock::now();
      hasher[0].hash(queries[i], &result[0]);
      auto ttt2 = high_resolution_clock::now();
      result_.hash_time += duration_cast<duration<double>>(ttt2 - ttt1).count();
      uint32_t num_candidates = 0;
      auto tttt1 = high_resolution_clock::now();
      for (size_t j = 0; j < num_tables; ++j) {
	uint32_t u = 0, v = 0;
	uint32_t cur = 1;
	uint32_t pos = result[0][j] * BYTE_WIDTH;
	for (size_t k = 0; k < BYTE_WIDTH; ++k) {
	  u += tables[j].start_buf[pos++] * cur;
	  cur *= 256;
	}
	cur = 1;
	for (size_t k = 0; k < BYTE_WIDTH; ++k) {
	  v += tables[j].start_buf[pos++] * cur;
	  cur *= 256;
	}
	pos = u * BYTE_WIDTH;
	for (size_t k = 0; k < v - u; ++k) {
	  uint32_t cand = 0;
	  cur = 1;
	  for (size_t l = 0; l < BYTE_WIDTH; ++l) {
	    cand += tables[j].points_buf[pos++] * cur;
	    cur *= 256;
	  }
	  if (flag[cand] != i) {
	    flag[cand] = i;
	    if (num_candidates == candidates.size()) {
	      candidates.push_back(make_pair(0.0, 0));
	    }
	    candidates[num_candidates++] = make_pair(0.0, cand);
	  }
	}
      }
      auto tttt2 = high_resolution_clock::now();
      result_.table_time += duration_cast<duration<double>>(tttt2 - tttt1).count();
      result_.num_candidates += candidates.size();
      auto tt1 = high_resolution_clock::now();
      for (size_t j = 0; j < num_candidates; ++j) {
	if (j + 1 < num_candidates) {
	  __builtin_prefetch(&dataset[candidates[j + 1].second]);
	}
	if (j + 2 < num_candidates) {
	  __builtin_prefetch(&dataset[candidates[j + 2].second]);
	}
	candidates[j].first = (dataset[candidates[j].second] - queries[i]).squaredNorm();
      }
      auto tt2 = high_resolution_clock::now();
      result_.distance_time += duration_cast<duration<double>>(tt2 - tt1).count();
      nth_element(candidates.begin(), candidates.begin() + 9, candidates.begin() + num_candidates);
      set<uint32_t> aux;
      for (size_t j = 0; j < 10; ++j) {
	aux.insert(candidates[j].second);
      }
      for (size_t j = 0; j < 10; ++j) {
	if (aux.count(answers[i][j])) {
	  ++score;
	}
      }
    }
    auto t2 = high_resolution_clock::now();
    result_.query_time = duration_cast<duration<double>>(t2 - t1).count() / queries.size();
    result_.hash_time /= queries.size();
    result_.table_time /= queries.size();
    result_.distance_time /= queries.size();
    result_.accuracy = double(score) / (10.0 * queries.size());
    result_.num_candidates /= queries.size();
  }
  return result_;
}

int main(int argc, char **argv) {
  try {
    assert(argc == 3);
    int num_bits = atoi(argv[1]);
    int num_tables = atoi(argv[2]);
    vector<Point> dataset, queries;
    vector<vector<int>> answers;

    read_dataset("dataset.dat", &dataset);
    read_dataset("queries.dat", &queries);
    read_answers("answers.dat", &answers);

    normalize(&dataset); 

    Point center(dataset[0].size());
    center.setZero();
    for (size_t i = 0; i < dataset.size(); ++i) {
      center += dataset[i];
    }
    center /= dataset.size();
    for (size_t i = 0; i < dataset.size(); ++i) {
      dataset[i] -= center;
    }

    Statistics s = evaluate(dataset, center, queries, answers, num_bits, num_tables);
    cout << s << endl;
  }   
  catch (runtime_error &e) {
    cerr << "Runtime error: " << e.what() << endl;
    return 1;
  }
  catch (exception &e) {
    cerr << "Exception: " << e.what() << endl;
    return 1;
  }
  catch (...) {
    cerr << "ERROR" << endl;
    return 1;
  }
  return 0;
}
