#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "immintrin.h"

#define N1 1000000
#define N2 1000000
#define D 128
#define SZ1 32
#define SZ2 32

double get_time(void) {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec * 1000000.0 + tv.tv_usec;
}

int main(void) {
  assert((D * sizeof(float)) % 32 == 0);
  srand(time(NULL));
  void *aux;
  posix_memalign(&aux, 32, N1 * D * sizeof(float));
  float *a = (float*)aux;
  posix_memalign(&aux, 32, N2 * D * sizeof(float));
  float *b = (float*)aux;
  for (int i = 0; i < N1 * D; ++i) {
    a[i] = (rand() + 0.0) / (RAND_MAX + 1.0); 
  }
  for (int i = 0; i < N2 * D; ++i) {
    b[i] = (rand() + 0.0) / (RAND_MAX + 1.0); 
  }
  float *best = (float*)malloc(N1 * sizeof(float));
  int *who = (int*)malloc(N1 * sizeof(float));
  for (int i = 0; i < N1; ++i) {
    best[i] = -1e100;
    who[i] = -1;
  }
  float score;
  __m256 block_score, tmp_block, tt1, tt2;
  posix_memalign(&aux, 32, 8 * sizeof(float));
  float *buf = (float*)aux;
  double t1 = get_time();
  int last = 0;
  for (int ii = 0; ii < N1; ii += SZ1) {
    int u = ii + SZ1;
    if (u > N1) {
      u = N1;
    }
    if (ii >= last + 100) {
      double t2 = get_time();
      printf("%d %lf\n", ii, (t2 - t1) / 1000000.0);
      fflush(stdout);
      last = ii;
    }
    for (int jj = 0; jj < N2; jj += SZ2) {
      int uu = jj + SZ2;
      if (uu > N2) {
	uu = N2;
      }
      for (int i = ii; i < u; ++i) {
	for (int j = jj; j < uu; ++j) {
	  block_score = _mm256_set_ps(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	  for (int t = 0; t < D; t += 8) {
	    tt1 = _mm256_load_ps(a + i * D + t);
	    tt2 = _mm256_load_ps(b + j * D + t);
	    block_score = _mm256_fmadd_ps(tt1,
					  tt2,
					  block_score);
	  }
	  _mm256_store_ps(buf, block_score);
	  tmp_block = _mm256_permute2f128_ps(block_score, block_score, 1);
	  block_score = _mm256_add_ps(block_score, tmp_block);
	  block_score = _mm256_hadd_ps(block_score, block_score);
	  block_score = _mm256_hadd_ps(block_score, block_score);
	  _mm256_store_ps(buf, block_score);
	  score = buf[0];
	  if (score > best[i]) {
	    best[i] = score;
	    who[i] = j;
	  }
	}
      }
    }
  }
  double t2 = get_time();
  printf("time: %lf\n", (t2 - t1) / 1000000.0);
  int total = 0;
  for (int i = 0; i < N1; ++i) {
    total += who[i];
  }
  printf("%d\n", total);
  free(a);
  free(b);
  free(buf);
  return 0;
}
